package fr.bluescale.storage.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.bluescale.storage.model.User;
import fr.bluescale.storage.model.dao.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {
	@PersistenceContext
	private EntityManager em;

	@Override
	public User findUserByusername(String username) {
		// TODO Auto-generated method stub
		TypedQuery<User> query = em.createQuery("select u from User u where u.username = :username", User.class);
		query.setParameter("username", username);

		return query.getSingleResult();
	}

}
