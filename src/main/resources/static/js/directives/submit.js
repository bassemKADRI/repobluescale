app.directive('submitForm', [function() {
   return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
         $(elem).submit();
      }
   };
}]);